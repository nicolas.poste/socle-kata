List of katas on [CodingDojo](http://codingdojo.org/kata/)


# The Three Laws of TDD as seen by Robert C. Martin (Uncle Bob):

1. You must write a failing test before you write any production code.
2. You must not write more of a test than is sufficient to fail, or fail to compile.
3. You must not write more production code than is sufficient to make the currently failing test pass.


Create a unit tests that fails.  
Write production code that makes that test pass.  
Clean up the mess you just made.

See [Clean Coder](https://blog.cleancoder.com/uncle-bob/2014/12/17/TheCyclesOfTDD.html)
